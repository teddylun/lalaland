# lalaland
Master branch is finialized works. Most of my works is in the [reactjs](./reactjs) folder

### Prerequisites
``` 
node@7.6.0
```

### Getting Started
```
git clone https://teddylun@bitbucket.org/teddylun/lalaland.git
cd lalaland
```
and run mock APIs
```
cd mockApi
npm i
npm start
```
and run client
```
cd reactjs
npm i
npm start
```

### Getting Started for Docker lover
```
git clone https://teddylun@bitbucket.org/teddylun/lalaland.git
cd lalaland
docker-compose up --build
```
#### Docker will build and run the react app on [http://localhost:3000/](http://localhost:3000/) and mockapi [http://localhost:8080/](http://localhost:8080/) by ```docker-compose up --build```

### Run All Tests
- initialized the test for rendering core containers: 
[App.js](./reactjs/src/App.test.js) 
[home.js](./reactjs/src/containers/home.test.js)
```
cd reactjs
npm test 
a
```
with coverage report
```
cd reactjs
npm test -- --coverage
```

### Run Packages Analyze
```
cd reactjs
npm run analyze
```

### Foler structure
* [reactjs: client root](./reactjs)
	* [public: public assets folder](./reactjs/public)	
	* [src: app src folder](./reactjs/src)
        * [App.js: root component](./reactjs/src/App.js)
		* [components: presentational components folder](./reactjs/src/components)
		* [containers: container components folder](./reactjs/src/containers)
		* [utils: helpers functions folder](./reactjs/src/utils)

### How to use
![Scheme](assets/howto.png)
![Scheme](assets/200.png)
![Scheme](assets/input.png)
![Scheme](assets/mobile.png)		

### Authors
* **Teddy Lun** - *Initial work* - https://github.com/teddylun