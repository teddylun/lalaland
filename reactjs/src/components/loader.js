import React from 'react'
import ReactLoading from 'react-loading'

// loader when user click "Get Path" button
const Loader = ({type, color, height, width, className}) => (
  <div className="loader-wrap">
    <ReactLoading 
      type={type}
      color={color}
      height={height}
      width={width}
      className={className}
    />
  </div>
)

export default Loader