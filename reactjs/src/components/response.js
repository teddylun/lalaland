import React from 'react'
import { ListGroup, ListGroupItem } from 'react-bootstrap'
import './response.css'

const stringMassaging = (str) => {
  if(str){
    let firstStr = str.split("_")[0]
    let secondStr = str.split("_")[1]
    firstStr = firstStr.charAt(0) + firstStr.slice(1).toLowerCase()
    secondStr = secondStr.toLowerCase()
    return firstStr + " " + secondStr
  }
  return null
}

// Generic response component to display success or failure response
const Response = (props) => {
  const { response } = props
  const { status, error, total_distance, total_time } = props.response
  return (
    <div className="response">
      <ListGroup>
        {status ?
          <ListGroupItem header={status.charAt(0).toUpperCase() + status.slice(1)} bsStyle={status !== 'failure' && status !== 'ZERO_RESULTS' ? 'success' : 'danger'}>
            {error && error}
            {total_distance && `🚚 Total Distance: ${total_distance}\n`}
            {total_time && `⏰ Total Time: ${total_time}`}
          </ListGroupItem> : 
          response && response.response === undefined ? 
          <ListGroupItem header={stringMassaging(response)} bsStyle="danger"/> :
          <ListGroupItem header={`😅 ${response.response.status}`} bsStyle="danger">
            {response.response.data}
          </ListGroupItem>}
      </ListGroup>
    </div>
  )
}

export default Response

