import React, {Component}from 'react'
import './locationForm.css'
import { Button, ButtonGroup } from 'react-bootstrap'
import PlacesAutocomplete from 'react-places-autocomplete'

// Location form contain control buttons and place auto complete field
class LocationForm extends Component {
  render() {
    const { locations, formName, submitLocations, addStop, removeStop, isLoading } = this.props
    const defaultStyles = {
      root: 'form-group default-group',
      input: 'form-control default-group',
      autocompleteContainer: 'my-autocomplete-container',
    }
    const defaultStyles2 = {
      root: 'form-group group--2',
      input: 'form-control group--2',
      autocompleteContainer: 'my-autocomplete-container',
    }
    return ( 
      <form id="locationForm" onSubmit={(e) => submitLocations(e)}>
        <div className="form-header">
          <h4 className="form-name">{formName}</h4>
          <div>
            <ButtonGroup className="btn-group">
              <Button className="btn-left" bsStyle="warning" onClick={addStop}>Add Stop</Button>
              <Button className="btn-right" bsStyle="warning" type="submit" disabled={isLoading === 1 ? true : false}>Get Path</Button>
            </ButtonGroup>
          </div>
        </div>
        <div className="inputs">
          {locations.map((location, i) =>  
            <div className="input-group" key={i}>
                <PlacesAutocomplete   
                  inputProps={location.inputProps}
                  classNames={locations.length > 2 ? defaultStyles2 : defaultStyles}
                  id={location.inputProps.id}
                />
                {locations.length > 2 ?
                  <span className="input-group-btn">
                    <Button
                      onClick={(e) => removeStop(i)}
                      className="btn btn-danger" type="button">
                      X
                    </Button>
                  </span> : null}
              </div>
          )}
        
        </div>
       
      </form>
    )
  }
}

export default LocationForm

