import React from 'react'
import { withGoogleMap, GoogleMap, DirectionsRenderer } from 'react-google-maps'

// initial google map on component ../containers/home.js mount
const GoogleMapPane = withGoogleMap(props => (
  <GoogleMap
    defaultZoom={11}
    defaultCenter={{ lat: 22.3964, lng: 114.1095 }}>
    {props.directions && <DirectionsRenderer directions={props.directions} />}
  </GoogleMap>
))

export default GoogleMapPane