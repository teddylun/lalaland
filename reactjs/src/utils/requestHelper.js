import axios from 'axios'

// create instance for base endpoint contructor
let instance = axios.create({
  baseURL: 'http://localhost:8080'
})

// generic request helper
const doRequest = async (configs) => {
  try {
    const res = await instance(configs)
    return res.data
  } catch (e) {
    return e
  }
}

// POST /route: Submit start point and drop-off locations
export const postStartOffLocations = async (locations) => {
  const configs = {
    method: 'post',
    url: '/route',
    data: locations
  }
  return await doRequest(configs)
}

// GET /route/<TOKEN>: Get shortest driving route
export const getDrivingRoute = async (token) => {
  const configs = {
    method: 'get',
    url: `/route/${token}`
  }
  return await doRequest(configs)
}
