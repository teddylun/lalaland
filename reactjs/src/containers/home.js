/*global google*/
import React, { Component } from 'react'
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import { postStartOffLocations, getDrivingRoute } from '../utils/requestHelper'
import GoogleMapPane from '../components/googleMapPane'
import LocationForm from '../components/locationForm'
import Response from '../components/response'
import Loader from '../components/loader'
import './home.css'

class App extends Component {
  
  state = {
    directions: null,
    locations: [],
    isLoading: -1,
  }

  constructor(props) {
    super()
    this.addStop = this.addStop.bind(this)
    this.removeStop = this.removeStop.bind(this)
    this.submitLocations = this.submitLocations.bind(this)  
  }
  
  componentDidMount() { 
    this.setDefaultLocationFields()  
  }

  // set default location fields when component did mount
  setDefaultLocationFields() {
    let initLocations = new Array(2)
    for (let i = 0; i < initLocations.length; i++) {
      let tmpObj = {
        lat: '',
        lng: '',
        inputProps: {
          id: Date.now()+i,
          value: '',
          placeholder: i === 0 ? 'Pick Up Location' : 'Drop Off Location',
          onChange: (address) => this.onChange(address),
          onBlur: (address) => this.onBlur(address),
          onSelect: (address, placeId) => this.onSelect(address, placeId),
          onError: () => console.log('onError')
        }
      }
      initLocations[i] = tmpObj
    }
    this.setState({locations: initLocations})
  }

  // add location input at tail of array
  addStop() {    
    this.setState((preState, props) => {
      const { locations } = preState
      locations[locations.length-1]['inputProps']['placeholder'] = 'Stop Location'
      let tmpObj = {
        lat: '',
        lng: '',
        inputProps: {
          id: Date.now(),
          value: '',
          placeholder: 'Drop Off Location',
          onChange: (address) => this.onChange(address),
          onBlur: (address) => this.onBlur(address),
          onSelect: (address, placeId) => this.onSelect(address, placeId),
          onError: () => console.log('onError')
        }
      }
      return{
        locations: [...preState.locations, tmpObj]
      }
    })
  }
  
  // remove index of location input  
  removeStop(index) { 
    this.setState((preState, props) => {
      const { locations } = preState              
      if(locations.length > 2) {
        // update placeholder
        if (index === locations.length-1) { // last input field
          locations[index-1]['inputProps']['placeholder'] = 'Drop Off Location'
        } else if (index === 0) { // first input field
          locations[index+1]['inputProps']['placeholder'] = 'Pick Up Location'
        }
      }
      return {
        locations: locations.filter((_, i) => i !== index)
      }
    }, () => {
      this.tryRenderPath()
    })
  }

  // udpate current selected input to state
  onSelect(address, placeId) {
    this.setState({
      onSelectTarget: address.currentTarget
    })
  }

  //update state input props address
  onChange(address) { 
    this.setState((preState, props) => {
      const { locations, onSelectTarget } = preState 
      locations.forEach((location, i) => {
        if(String(location.inputProps.id) === onSelectTarget.id){
          location.inputProps.value = address
        }
      }) 
      return{
        locations, 
        response: null 
      }
    })    
  }

  // get and set lat lng on blur
  async onBlur(address) {
    const { locations, onSelectTarget } = this.state
    
    locations.forEach(async (location, i) => {
      if (String(location.inputProps.id) === onSelectTarget.id) {
        try{
          const res = await this.getGeoCodeByAddress(address.target.value)
          location.lat = res.lat
          location.lng = res.lng
          this.setState({ locations }, () => this.tryRenderPath())
        } catch(e) { 
          throw e 
        }
      }
    })
  }

  // get lat and lng of location from user input
  async getGeoCodeByAddress(location) {
    try{
      const results = await geocodeByAddress(location)
      const latLng = await getLatLng(results[0])
      return latLng
    } catch (e) {
      this.setState({ response: e, isLoading: 0})
      return e
    }
  }

  // POST /route and GET /route/<TOKEN> and print mock api path when response 200
  async submitLocations(e) {
    e.preventDefault()
    this.setState({ isLoading: 1 })
    let BreakException = {}
    let tmpArrs = []
    try {
      const {locations} = this.state
      locations.forEach((location, i) => {
        let tmpArr = new Array(2)
        if(!location.lat || !location.lng) {
          BreakException = {
            response: {
              status: 'Empty location',
              data: 'Location cannot be empty',
              i: i
            }
          }
          this.setState({ response: BreakException, isLoading: 0 })
          throw BreakException  
        }
        tmpArr[0] = location.lat
        tmpArr[1] = location.lng
        tmpArrs.push(tmpArr) 
      })
      const res = await postStartOffLocations([])
      if(res.token){
        console.log(res.token)
        const routeRes = await getDrivingRoute(res.token)
        this.setState({ response: routeRes, isLoading: 0 }, () => {
          if(routeRes.path !== undefined)
            this.renderPath(routeRes.path)
        })
      } else {
        BreakException = res
        this.setState({ response: res, isLoading: 0 })
        throw BreakException
      }
    } catch (e) {
      this.setState({isLoading: 0}) 
      throw e
    }
  }

  // try render path once user input location
  tryRenderPath() {
    try {
      const { locations } = this.state
      let tmpArr = []
      locations.forEach((location, i) => {
        if(location.lat && location.lng){
          tmpArr.push([location.lat, location.lng])
        } 
      })
      this.renderPath(tmpArr)
    } catch (e) { 
      throw e 
    }
  }

  // render path in google map
  renderPath(locations) {
    if(locations.length > 0){
      let waypoints = []
      for (let i = 1; i < locations.length - 1; i++) {
        let waypoint = {
          location: new google.maps.LatLng(locations[i][0], locations[i][1])
        }
        waypoints.push(waypoint)
      }
      const DirectionsService = new google.maps.DirectionsService()
      let path = {
        origin: new google.maps.LatLng(locations[0][0], locations[0][1]),
        waypoints: waypoints,
        destination: new google.maps.LatLng(
          locations[locations.length-1][0], locations[locations.length-1][1]
        ),
        travelMode: google.maps.TravelMode.DRIVING,
      }
      DirectionsService.route(path, (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            origin: new google.maps.LatLng(locations[0][0], locations[0][0]),
            directions: result
          })
        } else {
          console.error(`error fetching directions ${result}`)
          this.setState({ 
            response: result
          })
        }
      })
    } 
  }

  render() {
    const { locations, response, isLoading } = this.state
    return (
      <div className="home">
        <div className="home-header">
          <img src='/imgs/lalamove_icon_300.png' className="home-logo" alt="logo" />
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col col-xs-12 col-sm-3">
              <LocationForm 
                isLoading={isLoading}
                locations={locations}
                formName="ROUTE / STOPS"
                addStop={this.addStop}
                removeStop={this.removeStop}
                submitLocations={this.submitLocations}
              />
              {isLoading === 0 ? 
                response && <Response 
                  response={response}
                /> : isLoading === 1 ?
                <Loader 
                    color="#EF6730" 
                    type="balls"
                    className="loader"/>
                 : null}
            </div>
            <div className="col col-xs-12 col-sm-9">
              <GoogleMapPane
                containerElement={
                  <div style={{ height: 'calc(100vh - 70px)' }} />
                }
                mapElement={
                  <div style={{ height: 'calc(100vh - 70px)' }} />
                }
                directions={this.state.directions}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App;
