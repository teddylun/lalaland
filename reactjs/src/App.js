import React, { Component } from 'react'
import HomeContainer from './containers/home'

class App extends Component {
 render() {
   return (
     <HomeContainer />
   )
 }
}

export default App
